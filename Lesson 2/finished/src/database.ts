import fetch from 'node-fetch';
let profiles:any = [{name:"First Profile",route:"35A",stop:"石蔭東邨"}];

export async function getRoute(route:string|undefined,stop:string|undefined){
    const query = `query($route:String,$stop:String){
        getRoute(route:$route,stop:$stop){
          id
          provider
          route
          bound
          orig_tc
          dest_tc
          orig_en
          dest_en
          orig_sc
          dest_sc
        }
    }`;
    const variables = {route,stop}
    const res = await fetch("https://graphql-data-source.netlify.app/.netlify/functions/server",{
        method:"POST",
        headers:{
            "content-type":"application/json"
        },
        body:JSON.stringify({query,variables})
    });
    const {data} = await res.json();
    return data.getRoute.map((d:any)=>({
        route:d.route,
        co:d.provider,
        bound:d.bound,
        orig_tc:d.orig_tc,
        dest_tc:d.dest_tc,
        orig_en:d.orig_en,
        dest_en:d.dest_en,
        orig_sc:d.orig_sc,
        dest_sc:d.dest_sc
    }));
}

export async function getStop(stop:string|undefined,route:string){
    const query = `query($route:String,$stop:String){
        getStop(route:$route,stop:$stop){
          stop
          bound
          provider
          name_tc
          name_en
          name_sc
        }
      }`;
    const variables = {route,stop}
    const res = await fetch("https://graphql-data-source.netlify.app/.netlify/functions/server",{
        method:"POST",
        headers:{
            "content-type":"application/json"
        },
        body:JSON.stringify({query,variables})
    });
    const {data} = await res.json();
    return data.getStop.map((d:any)=>({
        stop:d.stop,
        co:d.provider,
        bound:d.bound,
        route:route,
        name_tc:d.name_tc,
        name_en:d.name_en,
        name_sc:d.name_sc
    }))
}

export async function getProfiles(){
    // const query = `{
    //     getProfile
    //     {
    //       name
    //       route
    //       stop
    //     }
    //   }`
    //   const res = await fetch("https://graphql-data-source.netlify.app/.netlify/functions/server",{
    //     method:"POST",
    //     headers:{
    //         "content-type":"application/json"
    //     },
    //     body:JSON.stringify({query})
    // });
    // const {data} = await res.json();
    // profiles = data.getProfile;
    // return data.getProfile;
    return profiles;
}

export async function addProfile(newProfiles:any) {
    profiles = await getProfiles();
    for(let newProfile of newProfiles){
        profiles.push({
            name:newProfile.chatroomName,
            route:newProfile.defaultRoute,
            stop:newProfile.defaultStop
        });
    }
    return profiles;
}