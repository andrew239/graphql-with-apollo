import { ApolloServer } from "@apollo/server";
import {startStandaloneServer } from "@apollo/server/standalone";
import {ApolloServerPluginLandingPageDisabled} from "@apollo/server/plugin/disabled"
import {resolvers} from './resolvers.js';
import { readFileSync } from "fs";
const typeDefs = readFileSync("./src/typeDefs.graphql",{encoding:"utf-8"});

const server = new ApolloServer({
    typeDefs,
    resolvers,
    plugins:!process.env.NODE_NOV?[]:[ApolloServerPluginLandingPageDisabled()]
});

const {url} = await startStandaloneServer(server,{listen:{port:4000}});
console.log(`Lesson 2 GraphQL Server at ${url}`);