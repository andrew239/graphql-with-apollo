import {getRoute,getStop,getProfiles,addProfile} from './database.js';
import fetch from 'node-fetch';

export const resolvers = {
    Query:{
        getNextETA:async(_:any,params:any)=>{
            const {route,stopName} = params;
            const stopInfos = await getStop(stopName,route);

            let res:any = [];
            for (let stopInfo of stopInfos){
                const etaListRes = await fetch(`https://data.etabus.gov.hk/v1/transport/kmb/eta/${stopInfo.stop}/${route}/1`);
                const {data} = await etaListRes.json();
                const values = data.map((d:any)=>({
                    seq:d.seq,
                    co:"KMB",
                    bound:d.dir,
                    routeName:route,
                    stopName,
                    eta:d.eta
                }));
                res.push(...values);
            }
            return res;
        },
        getRoute:async(_:any,params:any)=>await getRoute(params.route,_),
        getStop:async (_:any,params:any)=>await getStop(params.stop,params.route),
        getProfiles:async()=>await getProfiles(),
        helloWorld:()=>"Hello World",
        getProfile:async()=>await getProfiles()[0]
    },
    Route:{
        stops:async(parent:any)=>await getStop(undefined,parent.route),
        orig:(parent:any,params:any)=>{
            switch (params.language) {
                case "EN":
                    return parent.orig_en;
                case "TC":
                    return parent.orig_tc;
                case "SC":
                    return parent.orig_sc;
                default:
                    break;
            }
        },
        dest:(parent:any,params:any)=>{
            switch (params.language) {
                case "EN":
                    return parent.dest_en;
                case "TC":
                    return parent.dest_tc;
                case "SC":
                    return parent.dest_sc;
                default:
                    break;
            }
        }
    },
    Stop:{
        routes:async(parent:any)=>await getRoute(undefined,parent.stop),
        name:(parent:any,params:any)=>{
            switch (params.language) {
                case "EN":
                    return parent.name_en;
                case "TC":
                    return parent.name_tc;
                case "SC":
                    return parent.name_sc;
                default:
                    break;
            }
        }
    },
    ETA:{
        route:async(parent:any)=>{
            const routes = await getRoute(parent.routeName,parent.stop);
            return routes.filter((route:any)=>route.bound === parent.bound)[0];
        },
        stop:async(parent:any)=>{
            const stops = await getStop(parent.stopName,parent.routeName);
            return stops.filter((stop:any)=>stop.bound === parent.bound)[0];
        }
    },
    Mutation:{
        addChatroomProfiles:(_: any,params:any)=>addProfile(params.profiles),
        addChatroomProfile:(_:any,params:any)=>addProfile([params])
    }
}
