//2 Points:
//1. No Resolvers, how to test now?
//2. Production : Is opened GraphQL Playground? Are you sure?
import { ApolloServer } from "@apollo/server";
import {startStandaloneServer} from '@apollo/server/standalone';
import { addMocksToSchema } from '@graphql-tools/mock';
import { makeExecutableSchema } from '@graphql-tools/schema';
import {ApolloServerPluginLandingPageDisabled} from '@apollo/server/plugin/disabled'
import { readFileSync } from "fs";
const typeDefs = readFileSync("./src/typeDefs.sample.graphql",{encoding:"utf-8"});

// const server = new ApolloServer({
//     typeDefs,
//     resolvers
// });
const server = new ApolloServer({
    schema:addMocksToSchema({
        schema:makeExecutableSchema({typeDefs}),
        mocks:{
            Int:()=>1,
            String:()=>"Test"
        }
    }),
    plugins:!process.env.NODE_ENV?[]:[ApolloServerPluginLandingPageDisabled()]
})
const {url} = await startStandaloneServer(server,{listen:{port:4000}});
console.log(`Lesson 2 GraphQL Server is started ${url}`);