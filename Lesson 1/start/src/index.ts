import {ApolloServer} from '@apollo/server';
import { startStandaloneServer } from '@apollo/server/standalone'

const typeDefs = `#graphql
type Query{
    helloWorld:String
    testInt:Int
}
`

const resolvers = {
    Query:{
        helloWorld:()=>"Hello World",
        testInt:()=>1
    }
}
const server = new ApolloServer({
    typeDefs,
    resolvers
});

const {url} = await startStandaloneServer(server);
console.log(`My first GraphQL started ${url}`);