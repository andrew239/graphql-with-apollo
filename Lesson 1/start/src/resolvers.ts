export const resolvers = {
    Query:{
        getNextETA:()=>[],
        getRoute:()=>[],
        getStop:()=>[],
        getProfiles:()=>[],
        helloWorld:()=>"Hello World",
        getProfile:()=>[]
    },
    Route:{
        stops:()=>[],
        orig:()=>[],
        dest:()=>[]
    },
    Stop:{
        routes:()=>[],
        name:()=>[]
    },
    ETA:{
        route:()=>[],
        stop:()=>[]
    },
    Mutation:{
        addChatroomProfiles:()=>[],
        addChatroomProfile:()=>[]
    }
}