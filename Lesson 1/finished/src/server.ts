// import {ApolloServer} from 'apollo-server';
// import {ApolloServerPluginLandingPageDisabled} from 'apollo-server-core';
// import {resolvers} from './resolvers';
// import {readFileSync} from "fs";
// const typeDefs = readFileSync("./typeDefs.graphql",{encoding:"utf-8"});


// const server = new ApolloServer({
//     typeDefs,
//     resolvers,
//     plugins:process.env.NODE_ENV?[ApolloServerPluginLandingPageDisabled()]:[],
// });
// server.listen().then(()=>{
//     console.log(`🚀 Server ready at http://localhost:4000${server.graphqlPath}`);
// });

import { ApolloServer } from "@apollo/server";
import {startStandaloneServer} from "@apollo/server/standalone";
import {resolvers} from './resolvers.js';
import { readFileSync } from "fs";
const typeDefs = readFileSync("./src/typeDefs.graphql",{encoding:"utf-8"});

const server = new ApolloServer({
    typeDefs,
    resolvers
});
const {url} = await startStandaloneServer(server);
console.log(`GraphQL Server is started at ${url}`);