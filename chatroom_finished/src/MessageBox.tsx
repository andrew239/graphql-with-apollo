import React from 'react'
import Avatar from './MyAvatar';
import {auth} from './firebase';
import {useAuthState} from 'react-firebase-hooks/auth';

function MessageBox({message}:any) {
    const [user] = useAuthState(auth);
    return (
        message.pid === "host"?(
            <div className='w-screen flex flex-row justify-start pl-2'>
                <div className='bg-blue-400 flex flex-row items-center p-2 w-fit ml-2 mt-2 rounded-xl'>
                    <Avatar name="Personal Assistant" />
                    <div className='ml-2'>{message.text}</div>
                </div>
            </div>
        ):
            (
                <div className='w-screen flex flex-row justify-end pr-2'>
                    <div className='bg-blue-400 flex flex-row items-center p-2 w-fit ml-2 mt-2 rounded-xl'>
                        <Avatar name={user?.displayName}/>
                        <div className='ml-2'>{message.text}</div>
                    </div>
                </div>
            )
    )
}

export default MessageBox
