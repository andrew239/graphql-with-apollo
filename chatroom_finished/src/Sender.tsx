import React, { useState,useEffect } from 'react'
import { TextField,Button } from '@mui/material'
import {Send} from 'iconsax-react';
import { auth } from './firebase';
import {useAuthState} from 'react-firebase-hooks/auth';
import {sendWelcomeMessage,sendMsg,getStats,setStats} from './Chatbot';
import {gql,useQuery,useLazyQuery} from '@apollo/client';
import moment from 'moment-timezone';

function Sender() {
    const [message, setMessage] = useState("");
    const [user] = useAuthState(auth);
    const { uid, photoURL } = auth.currentUser as any;

    //ChatBot
    async function stateMachine(auth:any,message:string,getETA:any,profiles:any,getRoute:any,route:any){
        if (getStats() === 0){
            if (message === "OK"){
                await sendMsg([message],photoURL,uid);
                getETA(
                    {
                        variables: {
                            route: profiles.data.getProfiles[0].route,
                            stopName: profiles.data.getProfiles[0].stop
                        }
                    }
                )
                setStats(-1);
            }else{
                getRoute({variables:{route:message}});
                setStats(1);
            }
        }else if (getStats() === 1){
            getETA(
                {
                    variables: {
                        route: route.data ?
                            route.data.getRoute[0].route:
                            profiles.data.getProfiles[0].route,
                        stopName: message
                    }
                }
            )
            setStats(-1);
        }else if (getStats() === -2){
            await sendMsg([message],photoURL,uid);
            await sendWelcomeMessage(profiles.data.getProfiles[0].route);
            setStats(0);
        }
    }

    //TODO : Get default route profile from GraphQL Server
    const profileQuery = gql`query GetProfiles {
            getProfiles {
                name
                route
                stop
            }
        }`;
    const profiles = useQuery(profileQuery);

    //TODO : Get Route from GraphQL Server
    const routeLazyQuery = gql`query getRoute($route:String){
        getRoute(route:$route) {
          route
        }
    }`;
    const [getRoute,route] = useLazyQuery(routeLazyQuery,{fetchPolicy:"no-cache"});
    useEffect(()=>{
        (async()=>{
            if (profiles.data){
                if (getStats() != -2) {
                    await sendMsg([route.data.getRoute.length > 0 ?
                        route.data.getRoute[0].route :
                        profiles.data.getProfiles[0].route], photoURL, uid);
                    await sendMsg(["請提供站頭名稱"], "", "host");
                }
            }
        })();
    },[route.data])

    //TODO : Get EAT from GraphQL Server
    const etaLazyQuery = gql`query GetNextETA($route: String, $stopName: String) {
        getNextETA(route: $route, stopName: $stopName) {
            bound
            eta
            stop {
                name_tc
            }
            route {
                route
                orig_tc
                dest_tc
            }
        }
      }`;
    const [getETA,eta] = useLazyQuery(etaLazyQuery,{fetchPolicy:"no-cache"});

    useEffect(() => {
        if (eta.data) {
            if (getStats() != -2) {
                const etaData = eta.data.getNextETA.map((item: any) => `往 ${item.route.dest_tc} ${moment(item.eta).format("hh:mm")}`);
                (async () => {
                    if (eta.data.getNextETA.length > 0) {
                        await sendMsg([eta.data.getNextETA[0].stop.name_tc], photoURL, uid);
                        await sendMsg(etaData, "", "host");

                        setStats(-2);
                        getRoute({ variables: { route: "End" } });
                    }
                })();
            }
        }
    }, [eta.data]);
  
   useEffect(() => {
        if (profiles.data){
            (async()=>await sendWelcomeMessage(profiles.data.getProfiles[0].route))();
        }
    }, [user,profiles.data]);
    return (
        <div className='bg-gray-200 pl-3 pt-3 pb-2 flex flex-row items-center'>
            <TextField className="flex-grow" 
                placeholder='Message' 
                variant="filled" value={message} 
                onChange={event=>setMessage(event.target.value)}
                onKeyPress={async(event:any)=>{
                        if(event.key === 'Enter'){
                            await stateMachine(auth,message,getETA,profiles,getRoute,route);
                            setMessage('');
                        }
                    }
                }/>
            <Button variant='text' onClick={async()=>{
                await stateMachine(auth,message,getETA,profiles,getRoute,route);
                setMessage('');
            }}>
                <Send />
            </Button>
        </div>
    )
}

export default Sender