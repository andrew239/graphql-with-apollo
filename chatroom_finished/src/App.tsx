import React from 'react';
import Chat from './Chat';
import "./index.css";
import {ApolloProvider,ApolloClient,InMemoryCache} from '@apollo/client'

const client = new ApolloClient({
  uri:process.env.REACT_APP_GRAPHQL_SERVER_URL,
  cache:new InMemoryCache()
});

function App() {
  return (
    <ApolloProvider client={client}>
      <Chat/>
    </ApolloProvider>
  );
}

export default App;
