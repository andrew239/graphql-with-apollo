import React from 'react'
import {GoogleAuthProvider, signInWithPopup} from 'firebase/auth';
import {auth} from './firebase';
import Button from '@mui/material/Button';

function SignIn() {
    const signInWithGoogle = async()=>{
        const provider = new GoogleAuthProvider();
        await signInWithPopup(auth,provider);
    }
    return (
        <div className='w-screen h-screen flex flex-row justify-center items-center'>
            <div className=' shadow-lg bg-blue-200 p-10 flex flex-col justify-center rounded-2xl'>
                <div className='mb-5 text-xl'>Personal Assistant Chatroom</div>
                <Button variant='contained' onClick={signInWithGoogle}>Sign In With Google</Button>
            </div>
        </div>
    )
}

export default SignIn
