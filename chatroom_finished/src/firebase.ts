// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries
import {getFirestore} from 'firebase/firestore';
import {getAuth} from 'firebase/auth';

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyBcO1RYDMH4wgC3fTw9xbWrvN7Ph_FXtPE",
    authDomain: "tecky-flex-course---graphql.firebaseapp.com",
    projectId: "tecky-flex-course---graphql",
    storageBucket: "tecky-flex-course---graphql.appspot.com",
    messagingSenderId: "518263191629",
    appId: "1:518263191629:web:1c6f974644e4a957290404",
    measurementId: "G-8GFLJJ78WQ"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);

//Add by yourself!
export const db = getFirestore(app);
export const auth = getAuth(app);