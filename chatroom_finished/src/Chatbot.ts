import { addDoc, collection, serverTimestamp } from 'firebase/firestore';
import { db } from './firebase';

//State Map
//Start Next Time : -1
//Welcome Message : 0
//Ask for Default Route/Specific Route : 1
//Complete the Request, take a break: 2
let state = 0;
let defaultRoute = "1A";

export async function sendWelcomeMessage(route:string){
    defaultRoute = route;
    await sendMsg([`主人，今日氣溫29.8°C。☀️☀️☀️`,`主人，你今日需要查詢巴士${route}到站時間？如果是，回「OK」如不是，請提供'上車地點 巴士路線'。`],"","host");
}

export async function sendMsg(texts:string[],photoURL:string,pid:string){
    for (let text of texts) {
        await addDoc(collection(db, "chatroom"), {
            text,
            photoURL,
            pid,
            createdAt: serverTimestamp()
        });
    }
}

export async function stateMachine(auth:any,message:string,getETA:any,profiles:any,getRoute:any,route:any){
    if (getStats() === 0){
        if (message === "OK"){
            const {uid,photoURL} = auth.currentUser as any;
            await sendMsg([message],photoURL,uid);
            getETA(
                {
                    variables: {
                        route: profiles.data.getProfiles[0].route,
                        stopName: profiles.data.getProfiles[0].stop
                    }
                }
            )
            setStats(-1);
        }else{
            getRoute({variables:{route:message}});
            setStats(1);
        }
    }else if (getStats() === 1){
        getETA(
            {
                variables: {
                    route: route.data ?
                        route.data.getRoute[0].route:
                        profiles.data.getProfiles[0].route,
                    stopName: message
                }
            }
        )
        setStats(-1);
    }else if (getStats() === -2){
        const {uid,photoURL} = auth.currentUser as any;
        await sendMsg([message],photoURL,uid);
        await sendWelcomeMessage(profiles.data.getProfiles[0].route);
        setStats(0);
    }
}

export const getStats = ()=>state;
export const setStats = (newState:number)=>{state = newState};