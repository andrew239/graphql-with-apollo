# Tecky Pro Course : GraphQL with Apollo
It is introduction course of GraphQL. In the course, student will develop the Personal Assistant Chatroom, so  that student can experience building the GraphQL Server and GraphQL Client using Apollo GraphQL Library.

## Course Prerequisite
* Understand RESTful API, AJAX and Node.js
* Experiencing in programming of Javascript and Typescript
* Experiencing React application development

## Topic each Lecture
* Lecture 1 - GraphQL Client
* Lecture 2 - GraphQL Server

## Course Source Code
[Source Code](https://gitlab.com/tecky.io/tecky-devs/graphql-with-apollo.git)

## PowerPoint Link
* [Lecture 1](https://docs.google.com/presentation/d/1syL0RVvtZdIucYPqYBHJ5YNj6soUFUfoCwXvcRn5Ogw/edit?usp=sharing)
* [Lecture 2](https://docs.google.com/presentation/d/1B3uTbhPWe8idd2cLvAnJqaCrBaVt7JesDGLuH-r1f4M/edit?usp=sharing)

## Course Materials
* [Lecture 1](https://andrew-tecky.gitbook.io/lesson-1/)
* [Lecture 2](https://andrew-tecky.gitbook.io/lesson-2/)

## Lecture Videos
The lecture videos will be uploaded after each lecture.
[Playlist]()