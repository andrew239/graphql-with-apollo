import { addDoc, collection, serverTimestamp } from 'firebase/firestore';
import { db } from './firebase';

//State Map
//Start Next Time : -1
//Welcome Message : 0
//Ask for Default Route/Specific Route : 1
//Complete the Request, take a break: 2
let state = 0;

export async function sendWelcomeMessage(){
    //TODO: Get default route profile from GraphQL Server
    await sendMsg(["主人，今日氣溫29.8°C。☀️☀️☀️","主人，你今日需要查詢巴士1A到站時間？如不是，請提供'上車地點 巴士路線'。"],"","host");
}

export async function sendMsg(texts:string[],photoURL:string,pid:string,queryHook:any = null){
    for (let text of texts) {
        await addDoc(collection(db, "chatroom"), {
            text,
            photoURL,
            pid,
            createdAt: serverTimestamp()
        });
    }
    if (state === 0){
        state = 1;
    }else if (state === 1){
        state = 2;
        //TODO: Get Route from GraphQL Server
        const route = undefined;
        if (route){
            //TODO: Get ETA from GraphQL Server
            queryHook();
            //TODO: sendMsg(<ETA>)
            await sendMsg(["15:36  15:52  16:06"],photoURL,pid);
        }else{
            //TODO: Get ETA from GraphQL Server from profile
            queryHook();
            //TODO: sendMsg(<ETA>)
            await sendMsg(["15:36  15:52  16:09"],photoURL,pid);
        }
    }else if(state === 2){
        state = -1;
    }else{
        sendWelcomeMessage();
        state = 0;
    }
}