import {auth} from './firebase';
import React, { useEffect } from 'react';
import Header from "./Header"
import Sender from "./Sender";
import MessageArea from './MessagesArea';
import SignIn from './SignIn';
import {useAuthState} from 'react-firebase-hooks/auth'

function Chat() {
  const [user] = useAuthState(auth);
  return (
    <>
      {
        user ? (<div className='flex flex-col h-screen'>
          <Header />
          <MessageArea />
          <Sender />
        </div>) : (<SignIn />)
      }
    </>
  )
}

export default Chat
