import React from 'react'
import Avatar from './MyAvatar';
import Button from '@mui/material/Button';
import {auth} from './firebase';
import {useAuthState} from 'react-firebase-hooks/auth';

function Header() {
    const [user] = useAuthState(auth);
    return (
        <>
             <div className='flex flex-row items-center justify-between bg-green p-4'>
                 <div className='flex flow-row justify-start items-center'>     
                    <Avatar name={user?.displayName}/>
                    <div className='ml-4 text-xl'>{user?.displayName}</div>
                 </div>
                 <Button variant='outlined' onClick={()=>{auth.signOut()}}>Sign Out</Button>
            </div>
        </>
    )
}

export default Header
