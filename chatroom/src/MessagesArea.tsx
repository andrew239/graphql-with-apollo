import { query, collection, orderBy, limit, onSnapshot } from 'firebase/firestore';
import React, { useEffect, useState,useRef } from 'react'
import { db ,auth} from './firebase';
import MessageBox from './MessageBox';
import {useAuthState} from 'react-firebase-hooks/auth';

function MessagesArea() {
    const scroll = useRef(null);
    const [messages,setMessages] = useState([]);
    const [user] = useAuthState(auth);
    const scrollToBottom = ()=>{
        (scroll.current as any).scrollIntoView({behavior:"smooth"});
    }
    useEffect(()=>{
        const data = query(collection(db,"chatroom"),orderBy("createdAt"),limit(50));
        onSnapshot(data,snapshot=>{
            setMessages(snapshot.docs.map(doc=>doc.data()) as any);
            scrollToBottom();
        });
    },[]);
    return (
        <div className='flex-grow'>
            {messages.filter((message:any)=>message.pid === "host" || message.pid === user?.uid).map((message:any,idx:number)=>(<MessageBox key={idx} message={message}/>))}
            <div ref={scroll}/>
        </div>
    )
}

export default MessagesArea