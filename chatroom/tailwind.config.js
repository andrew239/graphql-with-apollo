const colors = require('tailwindcss/colors')
module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {},
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      green: '#03fc8c',
      gray:colors.gray,
      blue:colors.blue
    }
  },
  variants: {
    extend: {},
  },
  plugins: [],
}